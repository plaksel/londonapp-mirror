require 'csv'

namespace :import do

	desc "Importing places from CSV"
	task places: :environment do 
		filename = File.join Rails.root, "places.csv"
		p filename

		CSV.foreach(filename, headers: true) do |row|
			p row
			p row["name"]
			place = Place.create(name: row["Name"], postcode: row["Postcode"], geo_longitude: row["geo_longitude"], geo_latitude: row["geo_latitude"], description: row["Description"], typePlace: row["Type"], price: row["Price (0 - 5)"], rating: row["Shagfactor"], placeID: row["Place ID"], romance: row["Romance (0 - 5)"], impressed: row["She Is Impressed(0 - 5)"], generalRating: row["General Impression (0 - 5)"], reachability: row["Reachability (0 - 5)"], comments: row["Comments"], imageUrl: row["imageUrl"], dateType: row["dateDay"], address: row["Address"], activityLevel: row["Activity Level (0 - 5)"], changeToTalk: row["Chance To Talk (0 - 5)"], descriptionGoogle: row["Description-Google"], drinksOrFood: row["dateType"] )		
		end
	end
end