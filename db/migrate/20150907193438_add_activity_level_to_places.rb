class AddActivityLevelToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :activityLevel, :string
    add_column :places, :changeToTalk, :string
  end
end
