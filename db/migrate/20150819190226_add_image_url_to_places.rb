class AddImageUrlToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :imageUrl, :string
  end
end
