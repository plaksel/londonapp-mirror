class AddGeoLatitudeToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :geo_latitude, :float
  end
end
