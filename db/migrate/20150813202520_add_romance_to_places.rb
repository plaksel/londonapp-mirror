class AddRomanceToPlaces < ActiveRecord::Migration
  def change
  	add_column :places, :romance, :integer
  	add_column :places, :impressed, :integer
  	add_column :places, :generalRating, :integer
  	add_column :places, :reachability, :integer
  end
end
