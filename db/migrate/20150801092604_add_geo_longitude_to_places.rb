class AddGeoLongitudeToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :geo_longitude, :float
  end
end
