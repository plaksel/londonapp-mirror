Rails.application.routes.draw do


  resources :places, :path => "london"
  resources :leads
  #resources :charges
  root to: 'places#index'
  get 'about' => 'places#about'
  post 'places/subscribe' => 'places#subscribe'
  #get 'london/about' => 'places#about'
  #root :to => redirect('/london')
  # get 'placelist' => 'places#list'
end
