class PlacesController < ApplicationController
  # GET /places
  # GET /places.json
  def about
  end
  
  def list
    @places = Place.all.order("rating DESC")
  end

  def index
    @lead = Lead.new
    @places = Place.all.order("rating DESC")
  end

  def subscribe
    @lead = Lead.new(lead_params) 
    respond_to do |format|

        if @lead.save
          format.html{redirect_to root_path, notice: "Saved successfully!"}
          format.js
        else
          format.html{redirect_to root_path, alert: "Failed to save"}
          format.js
        end
    end

    @lead._status

  end

  # GET /places/1
  # GET /places/1.json
  def show
    @place = Place.friendly.find(params[:id])

    respond_to do |format|
      format.html { } # show.html.erb
      format.json { render json: @place }
    end
  end

  def new
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_place
      @place = Place.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def place_params
      #params.require(:place).permit(:name, :description, :rating, :geo_latitude, :geo_latitude, :romance)
      params.require(:place).permit!
    end

    def lead_params
    params.require(:lead).permit(:email)
  end
end



