class LeadsController < ApplicationController
	def create
		@lead = Lead.new(lead_params)

		@list_id = 'c5baa21607'
		gb = Gibbon::API.new
		logger.debug @lead_email

		    response = gb.lists.subscribe({
		      :id => @list_id,
		      :email => {:email => @lead.email},
		      :double_optin => false
		      })

		    puts response

		respond_to do |format|

			if @lead.save
				format.html{redirect_to root_path, notice: "Saved successfully!"}
				format.js
				#format.json { render json: @lead }
			else
				format.html{redirect_to root_path, alert: "Failed to save"}
				format.js
				#format.json { render json: @lead.errors.full_messages, status: :unprocessable_entity }
			end
		end
	end

	private

	def lead_params
		params.require(:lead).permit(:email)
	end
end
