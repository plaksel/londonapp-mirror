$(document).ready ->

  $(document).bind "ajaxSuccess", "form.lead_form", (event, xhr, settings) ->
    $lead_form = $(event.data)
    $error_container = $("#error_explanation", $lead_form)
    $error_container_ul = $("ul", $error_container)
    $("<p>").html(xhr.responseJSON.title + " saved.").appendTo $lead_form
    if $("li", $error_container_ul).length
      $("li", $error_container_ul).remove()
      $error_container.hide()

  $(document).bind "ajaxError", "form.lead_form", (event, jqxhr, settings, exception) ->
    $lead_form = $(event.data)
    $error_container = $("#error_explanation", $lead_form)
    $error_container_ul = $("ul", $error_container)
    $error_container.show()  if $error_container.is(":hidden")
    $.each jqxhr.responseJSON, (index, message) ->
      $("<li>").html(message).appendTo $error_container_ul